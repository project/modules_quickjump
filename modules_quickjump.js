$(document).ready(function () {
    //Build the select element
    var select = $('<select name="quickjump"></select>')
            // Bind to change
            .change(function () {
                // scroll the page to the value stashed for the selected option 
                $("html:not(:animated),body:not(:animated)").animate({ scrollTop: $('option:selected', this).data('quickjump_scrollto')}, 500 );
                return false;
            })
            // wrap the select for the drupal menu
            .wrap('<li></li>')
            // insert it in the right place
            .appendTo($('ul.primary'));
    //Populate the select with options for each section
    // stash the position of the section using a data atttribute for later
    $('#system-modules fieldset legend a').each(function () {
        select.append($('<option>'+$(this).text()+'</option>').data('quickjump_scrollto', $(this).offset().top));
    });
});
